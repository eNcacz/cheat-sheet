# SSH

## Add server key to known\_hosts

```bash
ssh-keyscan -H hostname >> ~/.ssh/known_hosts
```

It resolves the "Host key verification failed." error from non-interactive scripts.

_You should verify obtained keys that they are really the proper one. You can allow MITM attack if you do not do it!_

## Remove key from known\_hosts

```bash
ssh-keygen -R hostname
```

It resolves the "REMOTE HOST IDENTIFICATION HAS CHANGED!" error.

_Of course you need to validate that the key on the server was really changed and you are not a target of MITM attack!_


